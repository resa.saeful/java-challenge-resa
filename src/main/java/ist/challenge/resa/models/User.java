package ist.challenge.resa.models;

import jakarta.persistence.*;
import org.hibernate.annotations.Where;


@Entity
@Where(clause = "is_delete = false")
@Table(name = "register")
public class User extends Common{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long Id;

    @Column(name = "username", length = 25, unique = true)
    private String Username;

    @Column(name = "email")
    private String Email;

    @Column(name = "password", length = 25)
    private String Password;

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String username) {
        Username = username;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }
}
