package ist.challenge.resa.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = "/login/")
public class LoginController {
    @GetMapping(value = "index")
    public ModelAndView login() {
        ModelAndView view = new ModelAndView("login/index");
        return view;
    }
}
