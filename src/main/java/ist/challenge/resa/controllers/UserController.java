package ist.challenge.resa.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
@Controller
@RequestMapping(value = "/user/")
public class UserController {
    @GetMapping(value = "index")
    public ModelAndView index() {
        ModelAndView view = new ModelAndView("user/index");
        return view;
    }
}
