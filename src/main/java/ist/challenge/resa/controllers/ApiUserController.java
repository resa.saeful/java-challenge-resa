package ist.challenge.resa.controllers;

import ist.challenge.resa.models.Login;
import ist.challenge.resa.models.User;
import ist.challenge.resa.repositories.UserRepo;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiUserController {
    @Autowired
    private UserRepo userRepo;

    @GetMapping("/user")
    public ResponseEntity<List<User>> GetAllUser() {
        try {
            List<User> user = this.userRepo.findAll();
            return new ResponseEntity<>(user, HttpStatus.OK);
        }
        catch (Exception ex)
        {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("user/{id}")
    public ResponseEntity<List<User>> GetUserById(@PathVariable("id")Long id)
    {
        try
        {
            Optional<User> user = this.userRepo.findById(id);
            if (user.isPresent())
            {
                ResponseEntity rest = new ResponseEntity(user, HttpStatus.OK);
                return rest;
            }
            else
            {
                return ResponseEntity.notFound().build();
            }
        }
        catch (Exception ex)
        {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }



    @PostMapping(value = "/user")
    public ResponseEntity<Object> SaveUser(@RequestBody User user)
    {
        try {
            user.setCreatedBy("Resa");
            user.setCreatedOn(new Date());
            this.userRepo.save(user);
            return new ResponseEntity<>("success", HttpStatus.valueOf(201));
        }
        catch (Exception ex)
        {
            return new ResponseEntity<>("Username sudah terpakai", HttpStatus.valueOf(409));
        }
    }

    @PutMapping("/user/{id}")
    public ResponseEntity<Object> UpdateUser(@RequestBody User user, @PathVariable("id") Long id)
    {
        try {
            Optional<User> userData = this.userRepo.findById(id);

            if (userData.isPresent())
            {
                user.setId(id);
                user.setModifiedBy("resa");
                user.setModifiedOn(new Date());
                this.userRepo.save(user);
                ResponseEntity rest = new ResponseEntity<>("success", HttpStatus.valueOf(201));
                return rest;
            }
            else
            {
                return ResponseEntity.notFound().build();
            }
        }
        catch (Exception ex)
        {
            return new ResponseEntity<>("Username sudah terpakai", HttpStatus.valueOf(409));
        }
    }

    @PostMapping(value = "/validate")
    public ResponseEntity<Object> ValidateUser(@RequestBody Login login, HttpSession session) {
        try {
            Optional<User> userData = this.userRepo.findByUsernamePassword(login.getUsername(), login.getPassword());
            if (userData.isPresent()) {
                session.setAttribute("id", userData.get().getId());
                session.setAttribute("username", userData.get().getUsername());
                session.setAttribute("email", userData.get().getEmail());
                return new ResponseEntity<>("success", HttpStatus.valueOf(200));
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            return new ResponseEntity<>("failed", HttpStatus.valueOf(401));
        }
    }

}
