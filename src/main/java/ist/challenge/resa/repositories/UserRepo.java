package ist.challenge.resa.repositories;

import ist.challenge.resa.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface UserRepo extends JpaRepository<User,Long> {
    @Query(value = "SELECT * FROM register e WHERE e.username = ?1 AND e.password = ?2", nativeQuery = true)       // using @query
    Optional<User> findByUsernamePassword(String email, String password);
}
