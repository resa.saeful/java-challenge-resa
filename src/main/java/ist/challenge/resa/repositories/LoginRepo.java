package ist.challenge.resa.repositories;

import ist.challenge.resa.models.Login;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface LoginRepo extends JpaRepository<Login,Long> {

}
